<?php
namespace Craft;

/**
 * Class LeadVariable
 * @package Craft
 */
class LeadsVariable
{
    /**
     * Return a list of all leads.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getLeads()
    {
        return craft()->leads->getLeads();
    }

    /**
     * Get any given lead.
     *
     * @param {string} $leadId | The lead ID.
     * @returns {object}
     *
     */
    public function getLead($leadId)
    {
        return craft()->leads->getLead($leadId);
    }
}