<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

$database_type = 'mysql';
$database_server = 'localhost';
$key = 'moss';
$database_user = $key;
$database_password = 'Jpl@57006';
$database_connection_charset = 'utf8';
$dbase = $key;
$staging = 'd_'  . $key;

return array(
    '*' => array(
        'tablePrefix' => 'craft',
    ),
    'local.' => array(
        'server' => '127.0.0.1',
        'user' => 'root',
        'password' => 'root',
        'database' => $key
    ),
    'beardedgingerdesigns.com' => array(
        'server' => 'localhost',
        'user' => $key,
        'password' => $database_password,
        'database' => $staging
        
    ),
    'mossdist.com' => array(
        'server' => 'localhost',
        'user' => $key,
        'password' => $database_password,
        'database' => $dbase
        
    ),
);