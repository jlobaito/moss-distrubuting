<?php

namespace Craft;

return [
	'endpoints' => [
		'api/categories.json' => function(){
			HeaderHelper::setHeader([
                'Access-Control-Allow-Origin' => '*'
            ]);

            return [
            	'elementType' => 'Category',
            	'criteria' => ['group' => 'gameType'],
				'transformer' => function(CategoryModel $category){

					$gameType = $category->title;
					$id = $category->id;

					return [
						'title' => $category->title,
						'gameType' => StringHelper::toKebabCase($gameType),
						'count' => $craft->entries->relatedTo($id)->total()
					];
				},

            ];

		},

		'api/products.json' => function() {

			HeaderHelper::setHeader([
                'Access-Control-Allow-Origin' => '*'
            ]);

            return [
			'elementType' => 'Entry',
			'criteria' => ['section' => 'products'],
			'elementsPerPage' => 10000000,
			'pageParam' => 'pg',
			'transformer' => function(EntryModel $entry){

				$image = $entry->image->first();
				$brand = $entry->brand->first();
				$gameType = $entry->gameType->first();
				$gameTypeCategories = [];
				foreach($entry ->gameType as $cat)
					$gameTypeCategories[] = $cat->title;
				return [
					'id' => $entry->id,
					'title' => $entry->title,
					'url' => $entry->url,
					'imageUrl' => $image ? $image -> getUrl('thumbnail') : null,
					'brand' => StringHelper::toKebabCase($brand),
					'gameType' => StringHelper::toKebabCase($gameType),
					'gameTypeCategories' => $gameTypeCategories

				];
			},

            ];


		}
	]
];
			
