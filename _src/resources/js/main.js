// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 

    },
    finalize: function() {

    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 

    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here

    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;

    // hit up common first.
    UTIL.fire( 'common' );

    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);


/* Youtube API */

function youTubeApi(target){
  var tag = document.createElement('script');
  var target = $(target);
  tag.src = "http://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  function onYouTubePlayerAPIReady() {
    var $m = $(target);
    player = new YT.Player('movie_player', {
      playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque', 'loop': 1, 'rel':0, 'showinfo':0, 'fs':0,'playlist':$m.data('video') },
      videoId: $m.data("video"),
      events: {
        'onReady': onPlayerReady}
      });
  }
}

// Sticky Elements

function sticky(element,height){
  var  mn = $(element);
    mns = "has-scrolled";
    hdr = $(height).height(),
    offset = $(element).outerHeight();

  $(window).scroll(function() {
    if( $(this).scrollTop() > 0 ) {
      if (!mn.hasClass(mns)){
        mn.addClass(mns);
       // mn.before('<div class="nav-offset" style="height:' + offset + 'px;"></div>' );
      }
      
    } else {
      mn.removeClass(mns);
      //$('.nav-offset').remove();
    }
  });
}
  
  

// Hide Header on on scroll down

function hasScrolled() {

  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $('header').outerHeight();
  console.log(navbarHeight);

  $(window).scroll(function(event){
    didScroll = true;
  });

  setInterval(function() {
    if (didScroll) {
      hasScrolled();
      didScroll = false;
    }
  }, 250);

  var st = $(this).scrollTop();
  // Make sure they scroll more than delta
  if(Math.abs(lastScrollTop - st) <= delta)
    return;
  
  // If they scrolled down and are past the navbar, add class .nav-up.
  // This is necessary so you never see what is "behind" the navbar.
  if (st > lastScrollTop && st > navbarHeight){
      // Scroll Down
      $('header').removeClass('nav-down').addClass('nav-up').css('top',-navbarHeight);
    } else {
      // Scroll Up
      if(st + $(window).height() < $(document).height()) {
        $('header').removeClass('nav-up').addClass('nav-down').css('top','0');
      }
    }

    lastScrollTop = st;
}

    /* Flex Destroy */
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*').removeAttr('style').removeClass(function (index, css) {
                return (css.match(/\bflex\S+/g) || []).join(' ');
            });

        elClean.insertBefore(el);
        el.remove();
    }

    /* Wrap Iframes in Responsive Emebed */
    function iframeEmbed(selector){
      var elem = $(selector);
      elem.wrap( "<div class='video-container'></div>" );
    }

    /* Velocity Page Load Animation */

    var isInView = function ($element) {
        var win = $(window);
        var obj = $element;
        var scrollPosition = win.scrollTop();
        var visibleArea = win.scrollTop() + win.height() + obj.data('offset') + 200;
        var objEndPos = (obj.offset().top + obj.outerHeight());

        return (visibleArea >= objEndPos && scrollPosition <= objEndPos ? true : false)
    };

  function animateContent(){
      $('div.builder-section').not('div.builder-section:first-child').each(function(){
        var offset = $(this).outerHeight() * 0.875;
        $(this).addClass('fade').attr('data-offset',offset);
      });

      $('.item').addClass('content-animate');

      $(window).ready(function(){
        $('.blog-index, .work-index, div.builder-section:first').addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
           stagger:150,
           delay:500,
           visibility:'visible'
        });
      });

      $(window).on('scroll load', function () {
        $('.blog-index, .work-index, div.builder-section').each(function (key, val) {
            var $el = $(this);

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

            if(!$el.hasClass("fade-in") && isInView($el)) {
              $el.addClass('fade-in').find('.content-animate').velocity('transition.slideRightIn',{
                 stagger:150,
                 delay:500,
                 visibility:'visible'
              });
            }

        });
    });

  }

/*
  Set Session Cookie
  */

  function sessionCookie(key){
    if ($.cookie(key) != 'true'){
      /*$('#popup').modal('show');*/
      $.cookie(key, 'true', { expires: 15 });
    }
  }


  

  function popupVideo(wrapper,element){
    var elem = $(element),
        wrapper = $(wrapper);

    $('.playVideo').click(function(){
      wrapper.velocity("fadeOut", { duration: 1000 })
      elem.velocity("fadeIn", {  delay: 999, duration: 1000 })
    })
  }

  // Mega Menu Area Hover
function subMenu(selector){
    $(selector).hover(
      function(){
      var elem = $(this).find('a'),
          tar = elem.data('hover');
      $('.main-menu li, .sub-menu').removeClass('active');
      $('#' + tar).addClass('active');
      $(this).addClass('active');
    }
    )
    $('nav').mouseleave(function(){
      $('.main-menu li, .sub-menu').removeClass('active');
    })
  }


// Cool Input Labels

function inputLabel(inputType){
    $(inputType).each(function(){
      var $this = $(this);
      // on focus add cladd active to label
      $this.focus(function(){
        $this.next().addClass("active");
      });
      //on blur check field and remove class if needed
      $this.blur(function(){
        if($this.val() === '' || $this.val() === 'blank'){
          $this.next().removeClass();
        }
      });
    });
  }
  

  function prettyList(elem){
    $(elem).wrapInner("<span></span>");
  }

  function imagePadding(){
    var height = $('.timeline-image').outerHeight();

    if ($(window).width() < 768) {
      $('.padding-bottom').css('padding-bottom', "5em")
    }
    else {
      $('.padding-bottom').css('padding-bottom', height + 75);
    }

  }

  // To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "dropdownFilter".

var dropdownFilter = {
  
  // Declare any variables we will need as properties of the object
  
  $filters: null,
  $reset: null,
  groups: [],
  outputArray: [],
  outputString: '',
  
  // The "init" method will run on document ready and cache any jQuery objects we will need.
  
  init: function(){
    var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "dropdownFilter" object so that we can share methods and properties between all parts of the object.
    
    self.$filters = $('#Filters');
    self.$reset = $('#Reset');
    self.$container = $('#Container');
    
    self.$filters.find('fieldset').each(function(){
      self.groups.push({
        $dropdown: $(this).find('select'),
        active: ''
      });
    });
    
    self.bindHandlers();
  },
  
  // The "bindHandlers" method will listen for whenever a select is changed. 
  
  bindHandlers: function(){
    var self = this;
    
    // Handle select change
    
    self.$filters.on('change', 'select', function(e){
      e.preventDefault();
      
      self.parseFilters();
    });
    
    // Handle reset click
    
    self.$reset.on('click', function(e){
      e.preventDefault();
      
      self.$filters.find('select').val('');
      
      self.parseFilters();
    });
  },
  
  // The parseFilters method pulls the value of each active select option
  
  parseFilters: function(){
    var self = this;
 
    // loop through each filter group and grap the value from each one.
    
    for(var i = 0, group; group = self.groups[i]; i++){
      group.active = group.$dropdown.val();
    }
    
    self.concatenate();
  },
  
  // The "concatenate" method will crawl through each group, concatenating filters as desired:
  
  concatenate: function(){
    var self = this;
    
    self.outputString = ''; // Reset output string
    
    for(var i = 0, group; group = self.groups[i]; i++){
      self.outputString += group.active;
    }
    
    // If the output string is empty, show all rather than none:
    
    !self.outputString.length && (self.outputString = 'all'); 
    
    //console.log(self.outputString); 
    
    // ^ we can check the console here to take a look at the filter string that is produced
    
    // Send the output string to MixItUp via the 'filter' method:
    
    if(self.$container.mixItUp('isLoaded')){
      self.$container.mixItUp('filter', self.outputString);
    }
  }
};


/**
 * @title BeLazy.js
 * @module BeLazy
 * @overview This module offers a utility to lazy load whatever you want on whatever action.
 * @copyright 2014, SpilGames
 */
(function (w, d, n) {
  'use strict';

  var BeLazy;

  if (!BeLazy) {
    var windowHeight = w.innerHeight || d.documentElement.clientHeight,
        // When resizing or scrolling, hundreds to thousands events can be send.
        // Instead of executing the listeners on every single event, we only
        // execute the logic every X miliseconds the configured values are
        // determined based on the research results from Ph.D. Steven C. Seow.
        resizeTimeout    = null,
        scrollTimeout    = null,
        resizeHandlerSet = false,
        scrollHandlerSet = false,
        listeners = {},
        listenersDone = 0,
        listenerCount = 0,

        addEventListener = (function () {
          var overwrite;
          if (w.addEventListener) {
            overwrite = function(type, listener, element) {
              element.addEventListener(type, listener, false);
            };
          } else if (w.attachEvent) {
            overwrite = function(type, listener, element) {
              element.attachEvent('on' + type, listener);
            };
          }

          return overwrite;
        })(),

        removeEventListener = (function () {
          var overwrite;
          if (w.removeEventListener) {
            overwrite = function(type, listener, element) {
              element.removeEventListener(type, listener, false);
            };
          } else if (w.detachEvent) {
            overwrite = function(type, listener, element) {
              element.detachEvent('on' + type, listener);
            };
          }

          return overwrite;
        })(),

        getTopOffset = function(element) {
          var offset = 0;
          if (typeof element.offsetParent === 'object') {
            while (element && typeof element.offsetParent === 'object') {
              offset += element.offsetTop;
              element = element.offsetParent;
            }
          } else {
            offset = element.offsetTop;
          }

          return offset;
        },

        updateListeners = function () {
          if (listenerCount < 1) {
            return;
          }

          var scrollTop = w.pageYOffset || d.documentElement.scrollTop,
              listenerIndex,
              listener;

          for (listenerIndex in listeners) {
            if (listeners.hasOwnProperty(listenerIndex)) {
              listener = listeners[listenerIndex];
              listener.offset = getTopOffset(listener.elem);

              if (!listener.handled && scrollTop >= (listener.offset - windowHeight)) {
                listenersDone += 1;
                listener.handled = true;

                listener.onready(listener.elem);
              }
            }
          }

          if (listenersDone === listenerCount) {
            removeEventListener('scroll', onScrollHandler, w);
            removeEventListener('resize', resetWindowHeight, w);
            scrollHandlerSet = false;
            resizeHandlerSet = false;
          }

          scrollTimeout = null;
        },

        onScrollHandler = function () {
          // Prevent massive js execution on fast/long scrolling.
          if (null === scrollTimeout) {
            scrollTimeout = w.setTimeout(function () {
              updateListeners();
            }, 50);// Fairly unnoticable number.
          }
        },

        resetWindowHeight = function () {
          if (null === resizeTimeout) {
            resizeTimeout = w.setTimeout(function () {
              windowHeight = w.innerHeight || d.documentElement.clientHeight;
              resizeTimeout = null;
              // Check if something became visible after the resize.
              onScrollHandler();
            }, 100);
          }
        },

        addScrollListener = function (element, listener) {
          listenerCount += 1;
          listeners[listenerCount] = {
            elem: element,
            onready: listener,
            offset: getTopOffset(element)
          };

          if (false === resizeHandlerSet) {
            addEventListener('resize', resetWindowHeight, w);
            resizeHandlerSet = true;
          }

          if (false === scrollHandlerSet) {
            if (n.userAgent.match(/webkit/i) && n.userAgent.match(/mobile/i)) {
              // iPad, iPhone, Android etc.
              addEventListener('touchmove', onScrollHandler, w);
            } else {
              addEventListener('scroll', onScrollHandler, w);
            }

            scrollHandlerSet = true;
          }

          // Directly check if the element is visible once added.
          onScrollHandler();

          return element;
        };

    BeLazy = {
      until: function(action, element, listener) {
        if (action === 'visible') {
          return addScrollListener(element, listener);
        }

        // Remove event after execution.
        var destructor = function () {
          removeEventListener(action, destructor, element);
          listener.call();
        };

        // Set the event listener.
        addEventListener(action, destructor, element);

        return element;
      }
    };
  }

  if (typeof define === 'function' && define.amd) {
    // AMD support.
    define(function () { return BeLazy; });
  } else if (typeof exports !== 'undefined') {
    // CommonJS support.
    exports.BeLazy = BeLazy;
  } else {
    w.BeLazy = BeLazy;
  }
})(window, window.document, window.navigator);










// Implementation of the BeLazy module.
(function(d) {
  'use strict';
  
  var i,
      images = d.getElementsByTagName('img'),
      regEx = /(^|\s+)lazyload(\s+|$)/;
  
  for (i = 0; i < images.length; i++) {
    // Check for `lazyload` class.
    if (regEx.test(images[i].className)) {
      (function(image) {
        image.onload = function() {
          // Remove `lazyload` class.
          image.className = image.className.replace(regEx, ' ');
        };

        // Add the BeLazy lazyloading.
        BeLazy.until('visible', image, function(image) {
          image.src = [
            image.getAttribute('data-src'),
            Math.random().toString(16).substr(2) // cache busting unique string
          ].join('');
        });
      })(images[i]);
    }
  }
})(window.document);



     function productMix(){
         // Instantiate MixItUp
            
        $('#Container').mixItUp({
          controls: {
            enable: false // we won't be needing these
          },
          callbacks: {
            onMixFail: function(){
              $('.mixFail').fadeIn();
            },
            onMixStart: function(){
              $('.mixFail').fadeOut();

            }
          }
        }); 
      }


  /* Init */

function pageLoad(){

Vue.use(infiniteScroll)


    Vue.use(VueResource);
    const vm = new Vue({
      el: '#app',
      delimiters: ['${', '}'],
      data: {
        products: [],
        brand:'',
        gameType:'',
        nextPage:0,
        showSpinner:1
      },
      mounted: function(){
        this.getProducts();
      },
      updated: function(){
        $('#Filters').fadeIn();
        productMix();
        vm.showSpinner = 0;
      },
      methods: {
        getProducts: function(){
          this.$http.get("api/products.json").then(function(data){
            this.products = this.products.concat(data.body.data);
          })
        }
      }
    });

    $('.news article').addClass('mix');
    $('.builder-newsAggregateSmall article').addClass('item');

      dropdownFilter.init();


      
       


      $('.main-form').submit(function(ev) {
        // Prevent the form from actually submitting
        ev.preventDefault();

        // Get the post data
        var data = $(this).serialize();

        // Send it to the server
        $.post('/', data, function(response) {
            if (response.success) {
                $('.main-form').fadeOut();
                $("#thank-you").fadeIn();

            } else {
                // response.error will be an object containing any validation errors that occurred, indexed by field name
                // e.g. response.error.fromName => ['From Name is required']
                alert('An error occurred. Please try again.');
            }
        });
    });

    $(".fancybox").fancybox();

     $('#toggle').click(function () {
            var windowSize = $(window).height();

            $('#overlay').css('height', windowSize);
            $(this).toggleClass('active');
            $('#overlay').toggleClass('open');
            $('html').toggleClass('open');
        });


    $(".gallery li").lazyload({
      effect : "fadeIn"
    });


    sticky('header','.header');

    imagePadding();

    $('.nav-button a').wrapInner('<span></span>');

    $('li.has-sub > a').attr('href',"");

    iframeEmbed('iframe');
    sessionCookie('visited');

    animateContent();

    prettyList('li');

    $('.builder-section').last().css('margin-bottom',0);

    //  $('.js-filter-event-game').on('change', function() {
    //     var val = $(this).val();

    //     // Hide all the events
    //     $('[data-game]').fadeOut();

    //     // Show the necessary events
    //     if (val == 'all') {
    //         $('[data-game]').fadeIn();
    //     } else {
    //         $('[data-game="'+val+'"]').fadeIn();
    //     }
    // });

    // $('.js-filter-event-brand').on('change', function() {
    //     var val = $(this).val();

    //     // Hide all the events
    //     $('[data-brand]').hide();

    //     // Show the necessary events
    //     if (val == 'all') {
    //         $('[data-brand]').fadeIn();
    //     } else {
    //         $('[data-brand="'+val+'"]').fadeIn();
    //     }
    // });

 
}


$(window).resize(function(){
  imagePadding();
  
})


$(document).ready(function(){
  pageLoad();
  
})